import Vue from 'vue'
import App from './App.vue'
import { GridGlobal } from 'gridjs-vue'

Vue.config.productionTip = false
Vue.use(GridGlobal)

new Vue({
  render: h => h(App),
}).$mount('#app')
